/* global describe, it, before, after */
'use strict'

const amqp = require('amqplib')
const should = require('should')
const moment = require('moment')
const uuid = require('uuid/v4')

const ID = Date.now()

let _app = null
let _conn = null
let _channel = null

/*let conf = {
  host: '127.0.0.1',
  port: '9042',
  user: 'cassandra',
  password: '',
  keyspace: 'reekoh',
  schema: {
    fields: {
      co2: {type: 'text'},
      created: {
        type: 'timestamp',
        default: {
          '$db_function': 'toTimestamp(now())'
        }
      },
      data_id: {
        type: 'bigint'
      },
      id: {
        type: 'uuid',
        default: {'$db_function': 'uuid()'}
      },
      is_normal: 'boolean',
      list: {
        type: 'list',
        typeDef: '<varchar>'
      },
      metadata: {
        type: 'map',
        typeDef: '<varchar, text>'
      },
      quality: 'float',
      random_data: 'varchar',
      reading_time: 'timestamp',
      set: {
        type: 'set',
        typeDef: '<varchar>'
      },
      temp: 'int'
    },
    key: [['id'], 'created'],
    clustering_order: {'created': 'DESC'},
    table_name: 'reekohtest11'
  }
}
*/

let record = {
  data_id: ID,
  co2: '11%',
  temp: 23,
  quality: 11.25,
  reading_time: '2015-11-27T11:04:13.000Z',
  metadata: {
    metadata_json: 'reekoh metadata json'
  },
  list: ['value1', 'value1', 'value2'],
  set: ['value1', 'value2'],
  random_data: 'abcdefg',
  is_normal: true
}

describe('Cassandra (v2) Storage', function () {
  // process.env.INPUT_PIPE = 'demo.pipe.storage'
  // process.env.BROKER = 'amqp://guest:guest@127.0.0.1/'
  // process.env.CONFIG = '{"host":"127.0.0.1","port":"9042","user":"cassandra","password":"","keyspace":"reekoh","schema":{"fields":{"co2":{"type":"text"},"created":{"type":"timestamp","default":{"$db_function":"toTimestamp(now())"}},"data_id":{"type":"bigint"},"id":{"type":"uuid","default":{"$db_function":"uuid()"}},"is_normal":"boolean","list":{"type":"list","typeDef":"<varchar>"},"metadata":{"type":"map","typeDef":"<varchar, text>"},"quality":"float","random_data":"varchar","reading_time":"timestamp","set":{"type":"set","typeDef":"<varchar>"},"temp":"int"},"key":[["id"],"created"],"clustering_order":{"created":"DESC"},"table_name":"reekohtest11"}}'

  let BROKER = process.env.BROKER
  let CONFIG = process.env.CONFIG
  let INPUT_PIPE = process.env.INPUT_PIPE

  before('init', () => {
    amqp.connect(BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).then((channel) => {
      _channel = channel
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate child process', function () {
    _conn.close()
  })

  describe('#start', function () {
    it('should start the app', function (done) {
      this.timeout(20000)
      _app = require('../app')
      _app.once('init', done)
    })
  })

  describe('#data', function () {
    it('should process the data', function (done) {
      this.timeout(10000)
      _channel.sendToQueue(INPUT_PIPE, new Buffer(JSON.stringify(record)))
      _app.on('processed', done)
    })
  })

  describe('#data', function () {
    it('should have inserted the data', function (done) {
      this.timeout(10000)

      let conf = JSON.parse(CONFIG)
      let models = require('express-cassandra')
      let modelPath = __dirname.slice(0, -4) + 'models'

      models.setDirectory(modelPath).bind({
        clientOptions: {
          contactPoints: `${conf.host}`.replace(/\s/g, '').split(','),
          keyspace: conf.keyspace,
          protocolOptions: {
            port: conf.port
          },
          queryOptions: {
            consistency: models.consistencies.one
          },
          username: 'cassandra',
          password: '',
        },
        ormOptions: {
          defaultReplicationStrategy: {
            class: 'SimpleStrategy',
            replication_factor: 1
          },
          migration: 'safe',
          createKeyspace: true
        }
      }, function (err) {
        if (err) throw err
        
          models.instance.Data.find({}, function(err, data){
          if(err) throw err

          for (var i = data.length - 1; i >= 0; i--) {
            if (parseInt(data[i].data_id) === parseInt(ID)) {
              should.equal(record.co2, data[i].co2, 'Data validation failed. Field: co2')
              should.equal(record.temp, data[i].temp, 'Data validation failed. Field: temp')
              should.equal(record.quality, data[i].quality, 'Data validation failed. Field: quality')
              should.equal(record.random_data, data[i].random_data, 'Data validation failed. Field: random_data')
              should.equal(moment(record.reading_time).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'), moment(data[i].reading_time).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'), 'Data validation failed. Field: reading_time')
              should.equal(JSON.stringify(record.metadata), JSON.stringify(data[i].metadata), 'Data validation failed. Field: metadata')
              should.equal(record.is_normal, data[i].is_normal, 'Data validation failed. Field: is_normal')
              done()
            }
          }
        })
      })
    })
  })
})
